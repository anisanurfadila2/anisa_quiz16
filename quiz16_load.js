import http from 'k6/http';
import { check , sleep } from 'k6';

export const options = {
  stages: [
    { duration: '1m', target: 100 },
    { duration: '2m', target: 100 },
    { duration: '30s', target: 20 },
    { duration: '10s', target: 0 },
  ],
};


export default function () {
  const res = http.get('https://run.mocky.io/v3/1b6d9480-44c4-49ae-a095-27e780edf0eb');
  check(res, { 'Status was 200': (r) => r.status == 200 });
  check(res, {
    'Status body is success': (r) =>
      r.body.includes('SUCCESS'),
  });
  sleep(1);
}